<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="index3.html" class="brand-link">
      <img src="{{ asset('themes').'/'.env('APP_THEMES') }}/dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
      <span class="brand-text font-weight-light">{{env('APP_NAME')}}</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="{{ asset('themes').'/'.env('APP_THEMES') }}/dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
            <a class="d-block">{{Session::get('admin')[0]['email']}}</a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
        <li>
            <a href="{{route('admin.home')}}" class="nav-link {{Request::url() == route('admin.home')  ? 'active' : ''}}">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Dashboard
              </p>
            </a>
        </li>
        <li class="nav-header">Modules</li>

        @php
                $role = \App\Models\Admin\User\RolesModel::where("role_id",Session::get('admin')[0]['role'])->first();
                $access_menu = json_decode($role->access_menu);
                if($role->role_id == 1){
                    $main_menu = \App\Models\Admin\User\MenuModel::where("parent_id",0)->where('status',1)->get();
                } else {
                    $main_menu = \App\Models\Admin\User\MenuModel::where("parent_id",0)->where('status',1)->whereIn('id',$access_menu)->get();
                }
                @endphp
                @foreach($main_menu as $mnu)
                @php
                if($role->role_id == 1){
                 $sub_menu = \App\Models\Admin\User\MenuModel::where("parent_id",$mnu->id)->where('status',1)->get();
                } else {
                 $sub_menu = \App\Models\Admin\User\MenuModel::where("parent_id",$mnu->id)->where('status',1)->whereIn('id',$access_menu)->get();   
                }
                $has_child = count($sub_menu) > 0 ? true : false;
                if($has_child){
                $check_active_menu = Str::contains(Request::url() , route($sub_menu[0]->link_url)) ? true : false;
                $parent_url = 'javascript:void(0)';
                } else {
                $check_active_menu = Str::contains(Request::url() , route($mnu->link_url)) ? true : false;
                $parent_url = route($mnu->link_url);
                }
                @endphp
                <li class="nav-item {{$check_active_menu ? 'menu-open' : ''}}">
                  <a href="{{$parent_url}}" class="nav-link {{$check_active_menu ? 'active' : ''}}">
                    <i class="nav-icon {{$mnu->icon}}"></i>
                    <p>
                    {{$mnu->name}}
                    @if($has_child)
                      <i class="right fas fa-angle-left"></i>
                    @endif
                    </p>
                  </a>
                  <ul class="nav nav-treeview">
                    @foreach($sub_menu as $sub_mnu)
                    <li class="nav-item">
                      <a href="{{route($sub_mnu->link_url)}}" class="nav-link {{Str::contains(Request::url() , route($sub_mnu->link_url)) ? 'active' : ''}}">
                        <i class="far fa-circle nav-icon"></i>
                        <p>{{$sub_mnu->name}}</p>
                      </a>
                    @endforeach
                    </li>
                  </ul>
                </li>
                @endforeach
        
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>