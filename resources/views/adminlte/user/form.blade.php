@extends(env('APP_THEMES').'.layout.main_layout.layout')
@section('style-css')
<!-- Select2 CSS -->
@endsection

@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>{{$head_title}}</h1>
          </div>
          
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            
		<div class="card">
			<div class="card-body">
				<div class="row">
					<div class="form-group col-lg-6">
						<label>Name</label>
						<input name="admin_id" type="hidden" value="{{isset($get_data->admin_id) ? $get_data->admin_id : ''}}">
						<input name="name" type="text" placeholder="Ex: John Doe" value="{{isset($get_data->name) ? $get_data->name : ''}}">
					</div>
					<div class="form-group col-lg-6">
						<label>Username</label>
						<input name="username" type="text" placeholder="Ex: johndoe1234" value="{{isset($get_data->username) ? $get_data->username : ''}}">
					</div>
					<div class="form-group col-lg-6">
						<label>Email</label>
						<input name="email" type="text" placeholder="Ex : johndoe@mail.com" value="{{isset($get_data->email) ? $get_data->email : ''}}">
					</div>
					<div class="form-group col-lg-6">
						<label>Role</label>
						<select name="role" class="select">
							<option>Select</option>
							@foreach($roles as $role)
							<option {{isset($get_data->role) ? ($get_data->role == $role->role_id ? "selected" : ""  ) : "" }} value="{{$role->role_id}}">{{$role->name}}</option>
							@endforeach
						</select>
					</div>
					<div class="form-group col-lg-6">
						<label>Mobile</label>
						<input name="phone" type="text" placeholder="08123456" value="{{isset($get_data->phone) ? $get_data->phone : ''}}">
					</div>
					@if($form_type == 'add')
					<div class="form-group col-lg-6">
						<label>Password</label>
						<div class="pass-group">
							<input name="password" type="password" class=" pass-input">
							<span class="fas toggle-password fa-eye-slash"></span>
						</div>
					</div>
					@endif
					<div class="col-lg-12">
						@if($form_type == 'add')
						<a id="submit_form" href="javascript:void(0);" class="btn btn-submit me-2">Submit</a>
						@else
						<a id="update_form" href="javascript:void(0);" class="btn btn-submit me-2">Update</a>
						@endif
					</div>
				</div>
			</div>
		</div>
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
@endsection
@section('script-js')
<!-- Select2 JS -->
<script>
	$(document).ready(function () {
		$('#submit_form').click(function(e){
                e.preventDefault();
                let name = $('[name=name]');
                let username = $('[name=username]');
                let email = $('[name=email]');
                let phone = $('[name=phone]');
                let role = $('[name=role]');
                let password = $('[name=password]');
                $.ajax({
                    type: "POST",
                    url: "{{ route('admin.user.lists.create') }}",
                    data: {
                        '_token': "{{ csrf_token() }}",
                        name:name.val(),
                        username:username.val(),
                        email:email.val(),
                        phone:phone.val(),
                        role:role.val(),
                        password:password.val(),
                    },
                    dataType:"JSON",
                    success: function(resp){
                        if(resp.status == "success"){
                            window.location.href ="{{route('admin.user.lists.index')}}";
                        }else{
                            Swal.fire("Failed!", "Submit Error!", "error");
                        }
                    },
                    error:function(data) {
                        Swal.fire("Failed!", "Submit Error!", "error");
                        return false;
                    }
            });
        });

        $('#update_form').click(function(e){
                e.preventDefault();
                let id = $('[name=admin_id]');
                let name = $('[name=name]');
                let username = $('[name=username]');
                let email = $('[name=email]');
                let phone = $('[name=phone]');
                let role = $('[name=role]');
                $.ajax({
                    type: "POST",
                    url: "{{ route('admin.user.lists.update') }}",
                    data: {
                        '_token': "{{ csrf_token() }}",
                        id:id.val(),
                        name:name.val(),
                        username:username.val(),
                        email:email.val(),
                        phone:phone.val(),
                        role:role.val(),
                    },
                    dataType:"JSON",
                    success: function(resp){
                        if(resp.status == "success"){
                            window.location.href ="{{route('admin.user.lists.index')}}";
                        }else{
                            Swal.fire("Failed!", "Update Error!", "error");
                        }
                    },
                    error:function(data) {
                        Swal.fire("Failed!", "Update Error!", "error");
                        return false;
                    }
            });
        });
	});
</script>
@endsection