@extends(env('APP_THEMES').'.layout.main_layout.layout')
@section('style-css')
<!-- DataTables -->
<link rel="stylesheet" href="{{ asset('themes').'/'.env('APP_THEMES') }}/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="{{ asset('themes').'/'.env('APP_THEMES') }}/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
<link rel="stylesheet" href="{{ asset('themes').'/'.env('APP_THEMES') }}/plugins/datatables-buttons/css/buttons.bootstrap4.min.css">
@endsection
@section('script-js')
<!-- DataTables  & Plugins -->
<script src="{{ asset('themes').'/'.env('APP_THEMES') }}/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="{{ asset('themes').'/'.env('APP_THEMES') }}/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="{{ asset('themes').'/'.env('APP_THEMES') }}/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="{{ asset('themes').'/'.env('APP_THEMES') }}/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="{{ asset('themes').'/'.env('APP_THEMES') }}/plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
<script src="{{ asset('themes').'/'.env('APP_THEMES') }}/plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
<script src="{{ asset('themes').'/'.env('APP_THEMES') }}/plugins/jszip/jszip.min.js"></script>
<script src="{{ asset('themes').'/'.env('APP_THEMES') }}/plugins/pdfmake/pdfmake.min.js"></script>
<script src="{{ asset('themes').'/'.env('APP_THEMES') }}/plugins/pdfmake/vfs_fonts.js"></script>
<script src="{{ asset('themes').'/'.env('APP_THEMES') }}/plugins/datatables-buttons/js/buttons.html5.min.js"></script>
<script src="{{ asset('themes').'/'.env('APP_THEMES') }}/plugins/datatables-buttons/js/buttons.print.min.js"></script>
<script src="{{ asset('themes').'/'.env('APP_THEMES') }}/plugins/datatables-buttons/js/buttons.colVis.min.js"></script>
<script>
  
	function deleteData(id){
          Swal.fire({
              title: 'Are you sure?',
              text: "",
              icon: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: 'Delete'
              }).then((result) => {
              if (result.isConfirmed) {
                  $.ajax({
                          type: "POST",
                          url: "{{ route('admin.user.lists.delete') }}",
                          data: {
                              '_token': "{{ csrf_token() }}",
                              id : id
                          },
                          dataType:"JSON",
                          success: function(resp){
                              if(resp.status == "success"){
                                Swal.fire(
                                  'Delete Success !',
                                  resp.message,
                                  'success'
                                ).then((res) => {
                                	$("#tabledata").DataTable().ajax.reload();
                                });
                                  
                              }else{
                                  Swal.fire("Failed!", "Delete Error!", "error");
                              }
                          },
                          error:function(data) {
                              Swal.fire("Failed!", "Delete Error!", "error");
                              return false;
                          }
                  }); 
              }
          })  

      }
	 $(document).ready(function () {
        var keyword = "";  
        var url = "{{ route('admin.user.lists.datatable') }}";

        const tableData = $("#tabledata").DataTable({
            "iDisplayLength"    : 25,
            "ordering"          : false,
            "processing"        : true,
            "serverSide"        : true,
            "dom"               : "rtip",
            ajax: function(data, callback, settings){
                $.get(url, {
                    limit: data.length,
                    offset: data.start,
                    keyword: keyword,
                    }, function(res) {
                        callback({
                            recordsTotal: res.recordsTotal,
                            recordsFiltered: res.recordsFiltered,
                            data: res.data
                        });
                    });
            },
            columns:[
                {data: "name"},
                {data: "email"},
                {data: "role"},
                {data: "aksi", render: function (data, type, row) {
                	var admin_id = row.admin_id;
                	var url_edit = '{{route("admin.user.lists.edit",["id"=>"admin_id"])}}';
                	url_edit = url_edit.replace('admin_id', admin_id);
                	var html = '';
                	html += '<a class="me-3" href="'+url_edit+'">'+
								'<i class="fas fa-edit"></i>'+
							 '</a> ';
					 html += '<a onclick="deleteData('+admin_id+')" class="me-3" href="javascript:void(0)">'+
                     '<i class="fas fa-trash"></i>'+
					 '</a>';
                    return html;
                 } },
            ]
        })

    });
</script>
@endsection
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>{{$head_title}}</h1>
          </div>
          
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                
			<div class="page-btn">
				<a href="{{route('admin.user.lists.add')}}" class="btn btn-added"><i class="fas fa-plus"></i> Add User</a>
			</div>
                <!-- <h3 class="card-title">DataTable with minimal features & hover style</h3> -->
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table id="tabledata" class="table table-bordered table-hover">
                  <thead>
                  <tr>
                    <th>Name </th>
                    <th>email</th>
                    <th>Role</th>
                    <th>Action</th>
                  </tr>
                  </thead>
                  <tbody>
                  
                  </tbody>
                  <tfoot>
                  <tr>
                    <th>Name </th>
                    <th>email</th>
                    <th>Role</th>
                    <th>Action</th>
                  </tr>
                  </tfoot>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
@endsection