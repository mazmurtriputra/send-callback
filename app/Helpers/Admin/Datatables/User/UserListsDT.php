<?php

    namespace App\Helpers\Admin\Datatables\User;
    use App\Models\Admin\User\ListsModel;
    use DB;
    use Session;

    use DataTables;

    class UserListsDT{
        protected $request;
        protected $keyword;

        protected $limit;
        protected $offset;

        public function __construct($request)
        {
            
            $this->request      = $request;
            $this->offset       = $request->get("offset");
            $this->limit        = $request->get("limit");
            $this->keyword      = $request->get("keyword");

        }


        function getTable(){
            
           // $helper = new AdminSessionHelper();
            $keyword = $this->keyword;
            $data = ListsModel::where(null);
            $data->where(function($query) use($keyword)
                {
                    $query->where("name"              , "REGEXP"     , $keyword);
                });

            return $data;
                                
        }

        public function getDataRow()
        {
            return $this->getTable()->offset($this->offset)->limit($this->limit);
        }

        public function getCount()
        {
            return $this->getTable()->count();
        }
        
    }