<?php

    namespace App\Helpers\Admin\Datatables\Sms;
    use App\Models\ReturnCallbackModel;
    use DB;
    use Session;

    use DataTables;

    class HistoryDT{
        protected $request;
        protected $keyword;

        protected $limit;
        protected $offset;

        public function __construct($request)
        {
            
            $this->request      = $request;
            $this->offset       = $request->get("offset");
            $this->limit        = $request->get("limit");
            $this->keyword      = $request->get("keyword");

        }


        function getTable(){
            
            $keyword = $this->keyword;
            $data = ReturnCallbackModel::where(null);
            $data->where(function($query) use($keyword)
                {
                    $query->where("message_id"              , "REGEXP"     , $keyword);
                });

            return $data;
                                
        }

        public function getDataRow()
        {
            return $this->getTable()->offset($this->offset)->limit($this->limit);
        }

        public function getCount()
        {
            return $this->getTable()->count();
        }
        
    }