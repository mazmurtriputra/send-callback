<?php

    namespace App\Helpers\Admin ;

    use App\Models\Admin\User\ListsModel as AdminLists;
    use Hash;
    use Session;

    class AdminAuthHelper{
        public function __construct()
        {
            
        }

        public function login($request)
        {
            $user = AdminLists::where("email", $request->post("email"))->first();
            if ($user) {
                if (Hash::check($request->post("password"), $user->password)) {
                    $session = [
                        "id" => $user->admin_id,
                        "username" => $user->username,
                        "email" => $user->email,
                        "role" => $user->role
                    ];
                    $session["permission_access"] = [];

                    Session::push("admin", $session);

                    $response = [
                        "status" => "success",
                        "message" => "Sign In Successfully!"
                    ];
                    return $response;

                }else {
                    $response = [
                        "status" => "failed",
                        "message" => "Password Doesn't Match!"
                    ];
                    return $response;
                }
            }else {
                $response = [
                    "status" => "failed",
                    "message" => "Account Not Found!"
                ];
                return $response;
            }
        }
    }