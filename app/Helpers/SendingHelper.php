<?php

namespace App\Helpers;

class SendingHelper
{
    public static function sendSms($data)
    {
        $end_point = 'http://103.251.44.86:20003/sendsms';

        $data['account'] = 'office';
        $data['password'] = '48ADD4FF';
        $data['content'] = $data['message'];
        $data['numbers'] = $data['destination'];
        
        $end_point = $end_point.'?account='.urlencode($data['account']).'&password='.urlencode($data['password']).'&content='.urlencode($data['content']).'&numbers='.urlencode($data['numbers']);
        $arrContextOptions=array(
            "ssl"=>array(
                "verify_peer"=>false,
                "verify_peer_name"=>false,
            ),
        );  


        $result = file_get_contents($end_point, false, stream_context_create($arrContextOptions));
        $responseObject = json_decode($result, true);
        return $responseObject;
    }

    public static function sendSmpp($data)
    {
        $end_point = $data['endpoint'].'?messageId='.urlencode($data['messageId']).'&status='.urlencode($data['status']).'&date='.urlencode($data['date']).'&destination='.urlencode($data['destination']).'&errorCode='.urlencode($data['error_code']);;

        $arrContextOptions=array(
            "ssl"=>array(
                "verify_peer"=>false,
                "verify_peer_name"=>false,
            ),
        );  

        $result = file_get_contents($end_point, false, stream_context_create($arrContextOptions));
        $responseObject = json_decode($result, true);
        return $responseObject;
    }    
}
