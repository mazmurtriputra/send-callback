<?php

namespace App\Http\Middleware\Admin;

use Closure;
use Illuminate\Http\Request;

class AuthenticateAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        $getSession = $request->session()->get("admin", []);

        
        if ( $request->url() == route('admin.auth.login') ) {
            if($getSession != null){
                return redirect(route('admin.home'));
            }else{
                return $next($request);
            }
            
        }else{
            if($getSession != null){
                return $next($request);
            }else{
                return redirect(route('admin.auth.login'));
            }
        }
        return $next($request);
    }
}
