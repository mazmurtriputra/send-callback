<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\TestingCallbackModel;
use App\Helpers\SendingHelper;
use App\Models\ReturnCallbackModel;



use DB;
use Carbon\Carbon;

class CALLBACKAPI extends Controller
{
    function sendCallback(Request $request){
        try {


            $random_number = str_pad(rand(0,9999), 5, "0", STR_PAD_LEFT);

            $data = [
                'message_id' => $random_number,
                'destination' => $request->post('destination'),
                'message' => $request->post('message'),
            ];

            $response = SendingHelper::sendSms($data);
            

            if (empty($data['destination'])) {
                return response()->json([
                    'status' => 'failed',
                    'message' => 'Destination Empty!!'
                ], 200);
            }
            
            $save_callback = [
                'message_id' => $data['message_id'],
                'destination' => $data['destination'],
                'message'  => $data['message'],
                'status'  => "PENDING",
                'provider_msg_id' => $response['array'][0][1],
            ];

            ReturnCallbackModel::create($save_callback);

            return response()->json([
                'status'      => 'success',
                'message'    => 'Sender Success!!',
                'data' => $response
            ],200);

        } catch (\Throwable $th) {
            return response()->json([
                'status'    =>  false,
                'message'   =>  'Error Throwable : '.$th->getMessage()
            ],500);
        }
    }

    function updateStatus(Request $request){
        try {

            $status = $request->post('status');
        
            TestingCallbackModel::where('id', $request->post('id'))
                                ->update(['status' => $status]);

            return response()->json([
                'status'      => 'success',
                'message'    => 'Update Success!!'
            ],200);

        } catch (\Throwable $th) {
            return response()->json([
                'status'    =>  false,
                'message'   =>  'Error Throwable : '.$th->getMessage()
            ],500);
        }
    }
}
