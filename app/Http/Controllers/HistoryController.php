<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Hash;
use Session;
use DataTables;
use App\Helpers\SendingHelper;
use App\Models\ReturnCallbackModel;


use App\Helpers\Admin\Datatables\Sms\HistoryDT as DataTableHelper;

class HistoryController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $data = [
            "title" => env("APP_NAME") . " : History",
            "head_tittle" => "History List",
        ];
        
        return view(env('APP_THEMES').'.history.index', $data);   
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(cr $cr)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(cr $cr)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, cr $cr)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(cr $cr)
    {
        //
    }

    public function sendHistory(Request $request)
    {


        $get = ReturnCallbackModel::find($request->id);

        $data = [
            'status' => "SENT",
            'messageId' => $get->message_id,
            'date' => $get->date,
            'destination' => $get->destination,
            'error_code' => 100,
            'endpoint' => "",
        ];

        $response = SendingHelper::sendSmpp($data);

        return response()->json([
            'status'      => 'success',
            'message'    => 'Sent Success'
        ],200);
    }

    public function get_datatable(Request $request)
    {
        $dataTableHelper = new DataTableHelper($request);
        return DataTables::of($dataTableHelper->getDataRow())
               ->make(true);
    }     
}
