<?php

namespace App\Http\Controllers\Callback;

use App\Http\Controllers\Controller;
use App\Models\TestingCallbackModel;
use App\Models\ReturnCallbackModel;


use Illuminate\Http\Request;

class Testing extends Controller
{
    public function test_callback(Request $request)
    {

        $params = $request->all();
        if(isset($params['type'])){
            if ($params['type'] == 'sent-sms') {
                
                $smsid = $params['sms_tid'];

                if ($params['status'] == 0 ) {
                    $status = "SENT";
                        $data = ReturnCallbackModel::where('provider_msg_id', $smsid)
                                                    ->update([
                                                    'status' => $status,
                                                    'delivered_date' => date("Y-m-d H:i:s"),
                                                    'done_date' => date("Y-m-d H:i:s")
                        ]);
                }  else {
                    $status = "FAILED";
                    $data = ReturnCallbackModel::where('provider_msg_id', $smsid)
                    ->update(['status' => $status,'done_date' => date("Y-m-d H:i:s")]);
                }


                $ins_array = [
                    'type'  => $params['type'],
                    'response'  => json_encode($params),

                ];
                TestingCallbackModel::create($ins_array);
            } 
        }
        
        return response()->json([
            'status'      => 'success',
            'message'    => 'Create Success!!'
        ],200);
        
        
    }
}

//https://wa1.otpcode.id/smsapi/dlroffice