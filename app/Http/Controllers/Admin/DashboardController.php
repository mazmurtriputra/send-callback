<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use DB;
use Session;

use App\Helpers\Admin\AdminSessionHelper;

class DashboardController extends Controller
{
    public function index(Request $request)
    {
        
        $data = [
            "title" => env("APP_NAME") . " : Dashboard",
            "menu" => "dashboard",
            "sub_menu" => ""
        ];
        
        return view(env('APP_THEMES').'.dashboard.index', $data);
    }
}
