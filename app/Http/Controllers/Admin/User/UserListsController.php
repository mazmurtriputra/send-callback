<?php

namespace App\Http\Controllers\Admin\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use DB;
use Hash;
use Session;
use DataTables;

use App\Helpers\Admin\AdminSessionHelper;
use App\Helpers\Admin\Datatables\User\UserListsDT as DataTableHelper;

use App\Models\Admin\User\ListsModel;
use App\Models\Admin\User\RolesModel;

class UserListsController extends Controller
{
    public function index(Request $request)
    {
        
        $data = [
            "title" => env("APP_NAME") . " : User Lists",
            "head_title" => "User Lists",
        ];
        
        return view(env('APP_THEMES').'.user.index', $data);
    }

    public function add(Request $request)
    {
        
        $data = [
            "title" => env("APP_NAME") . " : Add New User",
            "form_type" => "add",
            "head_title" => "User Management",
            "sub_title" => "Add User",
            "get_data" => null,
            "roles" => RolesModel::all(),
        ];
        
        return view(env('APP_THEMES').'.user.form', $data);
    }

    public function edit(Request $request,$id)
    {
        $chck = ListsModel::find($id);
        if($chck){
            $data = [
                "title" => env("APP_NAME") . " : Edit User",
                "form_type" => "edit",
                "head_title" => "User Management",
                "sub_title" => "Edit User",
                "get_data" => $chck,
                "roles" => RolesModel::all(),
            ];
            return view(env('APP_THEMES').'.user.form', $data);
        } else {
            return abort(404);
        }
    }

    public function create (Request $request)
    {
        $ins_array = [
            'name'  => $request->post('name') ,
            'username'  => $request->post('username') ,
            'phone'  => $request->post('phone') ,
            'email'  => $request->post('email') ,
            'role'  => $request->post('role') ,
            "password"   => Hash::make($request->post("password")),
        ];

        ListsModel::create($ins_array);
        
        return response()->json([
            'status'      => 'success',
            'message'    => 'Create Success!!'
        ],200);
    }

    public function update (Request $request)
    {
        $upd_array = [
            'name'  => $request->post('name') ,
            'username'  => $request->post('username') ,
            'phone'  => $request->post('phone') ,
            'email'  => $request->post('email') ,
            'role'  => $request->post('role') ,
        ];

        ListsModel::where('admin_id', $request->post('id'))
        ->update($upd_array);
        
        return response()->json([
            'status'      => 'success',
            'message'    => 'Update Success'
        ],200);
 
    }

    public function delete(Request $request)
    {
        $act = ListsModel::where('admin_id',$request->post('id'))->delete();
        
        return response()->json([
            'status'      => 'success',
            'message'    => 'Delete Success'
        ],200);
 
    }

    public function get_datatable(Request $request)
    {
        $dataTableHelper = new DataTableHelper($request);
        return DataTables::of($dataTableHelper->getDataRow())
               ->make(true);
    }     
}
