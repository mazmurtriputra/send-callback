<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use \App\Helpers\Admin\AdminAuthHelper;

class AuthController extends Controller
{
    public function login()
    {
        $data = [
            "title" => env("APP_NAME") . " : Login",
            "menu" => "auth",
            "sub_menu" => "login"
        ];
        return view(env('APP_THEMES').'.auth.login', $data);
    }
    
    public function actionLogin(Request $request)
    {
        $AuthAdmin = new AdminAuthHelper();
        $responseLogin = $AuthAdmin->login($request);
        return response()->json($responseLogin, 200);
    }

    public function logout(Request $request)
    {
        $request->session()->forget('admin');
        return redirect(route('admin.auth.login'));
    }
}
