<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ReturnCallbackModel extends Model
{
    use HasFactory;

    public $timestamps = false;
    protected $guarded = [];
    protected $primaryKey = "id";
    protected $table = "sms_history";

}
