<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TestingCallbackModel extends Model
{
    use HasFactory;

    public $timestamps = false;
    protected $guarded = [];
    protected $primaryKey = "callback_response_id";
    protected $table = "callback_response";


}
