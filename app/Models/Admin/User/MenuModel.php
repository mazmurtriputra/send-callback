<?php

namespace App\Models\Admin\User;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MenuModel extends Model
{
    use HasFactory;

    public $timestamps = false;
    protected $guarded = [];
    protected $primaryKey = "id";
    protected $table = "module_menu";

}
