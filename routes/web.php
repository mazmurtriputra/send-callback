<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\Admin\AuthController;
use App\Http\Controllers\Admin\DashboardController;
use App\Http\Controllers\Callback\Testing;
use App\Http\Controllers\Todo\TodoController;
use App\Http\Controllers\HistoryController;



use App\Http\Controllers\Admin\User\UserListsController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::middleware(['auth.admin'])->group(function () {
    Route::get('/login', [AuthController::class, 'login'])->name('admin.auth.login');
    Route::post('/login', [AuthController::class, 'actionLogin'])->name('admin.auth.a_login');
    Route::get('/logout', [AuthController::class, 'logout'])->name('admin.auth.logout');
    Route::get('/', [DashboardController::class, 'index'])->name('admin.home');


    Route::prefix('user_management')->group(function () {
        Route::prefix('lists')->group(function () {
            Route::get('/', [UserListsController::class, 'index'])->name('admin.user.lists.index');
            Route::get('/add', [UserListsController::class, 'add'])->name('admin.user.lists.add');
            Route::get('/edit/{id}', [UserListsController::class, 'edit'])->name('admin.user.lists.edit');
            Route::get('get_datatable', [UserListsController::class, 'get_datatable'])->name('admin.user.lists.datatable');
            Route::post('/create', [UserListsController::class, 'create'])->name('admin.user.lists.create');
            Route::post('/update', [UserListsController::class, 'update'])->name('admin.user.lists.update');
            Route::post('/delete', [UserListsController::class, 'delete'])->name('admin.user.lists.delete');
        });
    });

    Route::prefix('history')->group(function () {
         Route::get('/', [HistoryController::class, 'index'])->name('admin.history.index');
         Route::get('get_datatable', [HistoryController::class, 'get_datatable'])->name('admin.sms.history.datatable');
         Route::post('/sendHistory', [HistoryController::class, 'sendHistory'])->name('admin.sms.history.send');


    });
});

Route::prefix('callback')->group(function () {
    Route::prefix('testing')->group(function () {
        Route::post('/test_callback', [Testing::class, 'test_callback'])->name('callback.testing');
    });
});